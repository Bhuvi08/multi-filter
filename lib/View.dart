import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'Model.dart';

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  //API_function _api_functions=API_function();
  //Matinee_response data;

  TextEditingController firstnamecontroller = TextEditingController();
  TextEditingController middlenamecontroller = TextEditingController();
  TextEditingController lastnamecontroller = TextEditingController();
  TextEditingController agecontroller = TextEditingController();
  TextEditingController gendercontroller = TextEditingController();

  String first="";
  String last="";
  String middle="";
  String age="";
  String gender="";
  String first1="";
  String last1="";
  String middle1="";
  String age1="";
  String gender1="";

  List firstnamelist=[];
  List middlenamelist=[];
  List lastnamelist=[];
  List agelist=[];
  List genderlist=[];
  List AList=[];
  List WList=[];
  List XList=[];
  List YList=[];
  List ZList=[];

  bool matched=false;


  List<ListModel> listmodels = [
    ListModel(firstname: "Kapil", middlename: "Dev", lastname: "R", age: "30", gender: "Male",),
    ListModel(firstname: "Ram", middlename: "Rahim", lastname: "Singh", age: "25", gender: "Male",),
    ListModel(firstname: "Tamanna", middlename: "Batia", lastname: "Andra", age: "22", gender: "Female",),
    ListModel(firstname: "Kajal", middlename: "Agarwal", lastname: "Andra", age: "29", gender: "Female",),
    ListModel(firstname: "Ajay", middlename: "Kamal", lastname: "Rajni", age: "15", gender: "Child-Female",),
    ListModel(firstname: "Subash", middlename: "Manoharan", lastname: "TN", age: "24", gender: "Male",),
    ListModel(firstname: "Bhuvanesh", middlename: "Bhuvi", lastname: "G", age: "25", gender: "Baby",),
    ListModel(firstname: "Waran", middlename: "Bhuvi", lastname: "G", age: "22", gender: "Child-FeMale",),
    ListModel(firstname: "Kapil", middlename: "R", lastname: "TN", age: "30", gender: "Child-Male",),
    ListModel(firstname: "Ajith", middlename: "Vijay", lastname: "Surya", age: "30", gender: "Others",)
  ];

  uploadchange(String one,
      String two,
      String three,
      String four,
      String five
      )async{
    first1= one.toUpperCase();
    middle1= two.toUpperCase();
    last1= three.toUpperCase();
    age1= four;
    gender1= five.toUpperCase();
    if(first1=="" && middle1=="" && last1=="" && age1=="" && gender1==""){
      return Fluttertoast.showToast(
          msg: "Please enter atleast any one field",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          backgroundColor: Colors.black54,
          textColor: Colors.white);
    }
    else{
      for(int i=0; i<listmodels.length;i++){
        if(first1==""){
          AList.add(i);
        }else{
          if(firstnamelist[i]==first1){
            AList.add(i);
          }else{
          }
        }
      }
      for(int i=0; i<AList.length;i++){
        if(middle1==""){
          WList.add(AList[i]);
        }else{
          if(middlenamelist[AList[i]]==middle1){
            WList.add(AList[i]);
          }else{
          }
        }
      }

      for(int i=0; i<WList.length;i++){
        if(last1==""){
          XList.add(WList[i]);
        }else{
          if(lastnamelist[WList[i]]==last1){
            XList.add(WList[i]);
          }else{
          }
        }
      }

      for(int i=0; i<XList.length;i++){
        if(age1==""){
          YList.add(XList[i]);
        }else{
          if(agelist[XList[i]]==age1){
            YList.add(XList[i]);
          }else{
          }
        }
      }
      for(int i=0; i<YList.length;i++){
        if(gender1==""){
          ZList.add(YList[i]);
        }else{
          if(genderlist[YList[i]]==gender1){
            ZList.add(i);
          }else{
          }
        }
      }
      Navigator.of(context).pop();
    }
    setState(() {
      matched=true;
    });
  }

  datalist() async{
    //data= await _api_functions.getdata();
    setState(() {
      for(int i=0; i<listmodels.length;i++){
        firstnamelist.add(listmodels[i].firstname.toUpperCase());
      }
      for(int i=0; i<listmodels.length;i++){
        middlenamelist.add(listmodels[i].middlename.toUpperCase());
      }
      for(int i=0; i<listmodels.length;i++){
        lastnamelist.add(listmodels[i].lastname.toUpperCase());
      }
      for(int i=0; i<listmodels.length;i++){
        agelist.add(listmodels[i].age);
      }
      for(int i=0; i<listmodels.length;i++){
        genderlist.add(listmodels[i].gender.toUpperCase());
      }
    });

  }

  @override
  void initState() {
    datalist();
    super.initState();
  }

  @override
  void dispose() {
    firstnamelist.clear();
    lastnamelist.clear();
    middlenamelist.clear();
    agelist.clear();
    genderlist.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List_Filter"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10),
              child: GestureDetector(
                  onTap: (){
                    firstnamecontroller.clear();
                    middlenamecontroller.clear();
                    lastnamecontroller.clear();
                    agecontroller.clear();
                    gendercontroller.clear();
                    showAlertDialog(context);
                  },
                  child: Container(
                      color: Colors.blue,
                      width: 60,
                      height: 40,
                      child: Center(child: Text("Filter",style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),)))),
            ),
            Divider(
              thickness: 5,
            ),
            matched?ZList.isNotEmpty?Container(
              height: 500,
              child: ListView.builder(
                  itemCount: ZList.length,
                  itemBuilder: (contex, index) => Container(
                    color: index.isOdd?Colors.grey[200]:Colors.white,
                    height: 50,
                    child: ListTile(
                        onTap: () {
                          showAlertDialog2(context,ZList[index]);
                        },
                        title: Row(
                          children: [
                            Container(
                                width: 70,
                                child: Text(listmodels[ZList[index]].firstname)),
                            SizedBox(width: 5),
                            Container(
                                width: 70,
                                child: Text(listmodels[ZList[index]].middlename)),
                            SizedBox(width:5),
                            Container(
                                width: 65,
                                child: Text(listmodels[ZList[index]].lastname)),
                            SizedBox(width:5),
                            Container(
                                width: 35,
                                child: Text(listmodels[ZList[index]].age)),
                            SizedBox(width:5),
                            Container(
                                width: 65,
                                child: Text(listmodels[ZList[index]].gender)),
                          ],
                        )
                    ),
                  )
              ),
            ):Text("No details found"):Column(
              children: [
                Container(
                    height: 65,
                    child: Row(
                      children: [
                        Container(
                            width: 70,
                            child: Text("FirstName")),
                        SizedBox(width: 5),
                        Container(
                            width: 80,
                            child: Text("MiddleName")),
                        SizedBox(width:5),
                        Container(
                            width: 65,
                            child: Text("LastName")),
                        SizedBox(width:5),
                        Container(
                          alignment: Alignment.centerRight,
                            width: 40,
                            child: Text("Age")),
                        SizedBox(width:5),
                        Container(
                            width: 65,
                            child: Text("Gender")),
                      ],
                    )
                ),
                Container(
                  height: 500,
                  child: ListView.builder(
                      itemCount: listmodels.length,
                      itemBuilder: (context, index) => Container(
                        color: index.isOdd?Colors.grey[200]:Colors.white,
                        height: 50,
                        child: ListTile(
                            onTap: () {
                              showAlertDialog2(context,index);
                            },
                            title: Row(
                              children: [
                                Container(
                                    width: 70,
                                    child: Text(listmodels[index].firstname)),
                                SizedBox(width: 5),
                                Container(
                                    width: 80,
                                    child: Text(listmodels[index].middlename)),
                                SizedBox(width:5),
                                Container(
                                    width: 65,
                                    child: Text(listmodels[index].lastname)),
                                SizedBox(width:5),
                                Container(
                                    width: 30,
                                    child: Text(listmodels[index].age)),
                                SizedBox(width:5),
                                Container(
                                    width: 60,
                                    child: Text(listmodels[index].gender)),
                              ],
                            )
                        ),
                      )
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void showAlertDialog(BuildContext context) {
    showDialog(
      barrierDismissible:false,
      context: context,
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)),
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  height: MediaQuery.of(context).size.height/1.7,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'First Name',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50.8),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(MediaQuery.of(context).size.height / 71.2),
                            counterText: '',
                            errorStyle: TextStyle(height: 0),
                            border: InputBorder.none,
                            fillColor: Colors.grey[300],
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                                width: 2.0,
                              ),
                            ),
                          ),
                          controller: firstnamecontroller,
                        ),
                      ),
                      SizedBox(
                        height:20,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Middle Name',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50.8),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(MediaQuery.of(context).size.height / 71.2),
                            counterText: '',
                            errorStyle: TextStyle(height: 0),
                            border: InputBorder.none,
                            fillColor: Colors.grey[300],
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                                width: 2.0,
                              ),
                            ),
                          ),
                          controller: middlenamecontroller,
                        ),
                      ),
                      SizedBox(
                        height:20,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Last Name',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50.8),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(MediaQuery.of(context).size.height / 71.2),
                            counterText: '',
                            errorStyle: TextStyle(height: 0),
                            border: InputBorder.none,
                            fillColor: Colors.grey[300],
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                                width: 2.0,
                              ),
                            ),
                          ),
                          controller: lastnamecontroller,
                        ),
                      ),
                      SizedBox(
                        height:20,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'age',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50.8),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(MediaQuery.of(context).size.height / 71.2),
                            counterText: '',
                            errorStyle: TextStyle(height: 0),
                            border: InputBorder.none,
                            fillColor: Colors.grey[300],
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                                width: 2.0,
                              ),
                            ),
                          ),
                          controller: agecontroller,
                        ),
                      ),
                      SizedBox(
                        height:20,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Gender',
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          style: TextStyle(fontSize: MediaQuery.of(context).size.height / 50.8),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(MediaQuery.of(context).size.height / 71.2),
                            counterText: '',
                            errorStyle: TextStyle(height: 0),
                            border: InputBorder.none,
                            fillColor: Colors.grey[300],
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: Color(0xfff3f3f4),
                                width: 2.0,
                              ),
                            ),
                          ),
                          controller: gendercontroller,

                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
            actions: [FlatButton(
              onPressed:(){
                setState(() {
                  firstnamecontroller.clear();
                  middlenamecontroller.clear();
                  lastnamecontroller.clear();
                  agecontroller.clear();
                  gendercontroller.clear();
                  Navigator.of(context).pop();
                });
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius:BorderRadius.all(Radius.circular(20))),
                child: Center(child: Text("Cancel",style: TextStyle(fontSize: 14,color: Colors.white),)),
              ),
            ),
              FlatButton(
                onPressed:(){
                  setState(() {
                    AList.clear();
                    WList.clear();
                    XList.clear();
                    YList.clear();
                    ZList.clear();
                    first=firstnamecontroller.text;
                    last=lastnamecontroller.text;
                    middle=middlenamecontroller.text;
                    age=agecontroller.text;
                    gender=gendercontroller.text;
                    uploadchange(first,middle,last,age,gender);
                  });
                },
                child: Container(
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius:BorderRadius.all(Radius.circular(20))),
                  child: Center(child: Text("Submit",style: TextStyle(fontSize: 14,color: Colors.white),)),
                ),
              ),],
          ),
        );
      },
    );
  }

  void showAlertDialog2(BuildContext context,int i) {
    showDialog(
      barrierDismissible:false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius:
              BorderRadius.circular(20.0)),
          content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                height: MediaQuery.of(context).size.height/1.7,
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'First Name',
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Text(
                          listmodels[i].firstname
                      ),
                    ),
                    SizedBox(
                      height:20,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Middle Name',
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Text(
                          listmodels[i].middlename
                      ),
                    ),
                    SizedBox(
                      height:20,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Last Name',
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Text(
                          listmodels[i].lastname
                      ),
                    ),
                    SizedBox(
                      height:20,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Age',
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Text(
                          listmodels[i].age
                      ),
                    ),
                    SizedBox(
                      height:20,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Gender',
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      child: Text(
                          listmodels[i].gender
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
          actions: [FlatButton(
            onPressed:(){
              setState(() {
                Navigator.of(context).pop();
              });
            },
            child: Container(
              height: 40,
              width: 80,
              decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius:BorderRadius.all(Radius.circular(20))),
              child: Center(child: Text("Cancel",style: TextStyle(fontSize: 14,color: Colors.white),)),
            ),
          ),
            FlatButton(
              onPressed:(){
                setState(() {
                  Navigator.of(context).pop();
                });
              },
              child: Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius:BorderRadius.all(Radius.circular(20))),
                child: Center(child: Text("Submit",style: TextStyle(fontSize: 14,color: Colors.white),)),
              ),
            ),],
        );
      },
    );
  }
}
