class ListModel {
  String firstname;
  String middlename;
  String lastname;
  String age;
  String gender;

  ListModel({
    this.firstname,
    this.middlename,
    this.lastname,
    this.age,
    this.gender,
  });
}
